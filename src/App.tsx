import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route, Link, Routes } from "react-router-dom";
import { Index } from "./Components/Index";
import { Product } from "./Components/Product";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/products/1">First Product</Link>
              </li>
              <li>
                <Link to="/products/2">Second Product</Link>
              </li>
            </ul>
          </nav>
          <Routes>
            <Route path="/" Component={Index} />
            <Route path="/products/:id" Component={Product} />
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
