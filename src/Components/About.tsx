import React, { FC, ReactNode, memo } from "react";

const About: FC<{ temp: any }> = ({ temp }) => {
  console.log(temp);
  return <h2>About</h2>;
};

export default memo(About);
