import React, { FC, ReactNode, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useAppDispatch, RootState, AppDispatch } from "../Redux/Store";
import { useParams } from "react-router-dom";
import { addIssue } from "../Redux/Slice";
import { fetchIssues } from "../Redux/Thunk";
import About from "./About";
import { io } from "socket.io-client";
// import { decrement, increment } from "../Redux/Slice";

const socket = io("http://localhost:3001");

socket.on("connect", () => {
  console.log("Connected to the server");
});

socket.on("serverMessage", (message: any) => {
  const messagesDiv: any = document.getElementById("messages");
  const messageElement = document.createElement("div");
  messageElement.textContent = "Server: " + message;
  messagesDiv.appendChild(messageElement);
});

socket.on("disconnect", () => {
  console.log("Disconnected from the server");
});

export const Product: FC<any> = (props) => {
  const { id } = useParams();
  const user = useSelector((state: RootState) => state.githubIssue.user);
  const loading = useSelector((state: RootState) => state.githubIssue.loading);
  const error = useSelector((state: RootState) => state.githubIssue.error);
  const projectIssues = useSelector((state: RootState) => state.issue.projectIssues);

  const [state, setState] = useState<number>(0);

  const dispatch: AppDispatch = useAppDispatch();

  useEffect(() => {
    // dispatch(addIssue("11111"));

    dispatch(
      fetchIssues({
        email: "temadidenko27@gmail.com",
        password: "temich44F",
      })
    );
    console.log(1);
  }, []);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>Error: {error}</div>;
  }

  // const sendButton: any = document.getElementById("sendButton");
  const messageInput: any = document.getElementById("messageInput");

  const onClick = () => {
    const message = messageInput.value;
    socket.emit("clientMessage", message);

    const messagesDiv: any = document.getElementById("messages");
    const messageElement = document.createElement("div");
    messageElement.textContent = "Client: " + message;
    messagesDiv.appendChild(messageElement);

    messageInput.value = "";
  };

  return (
    <div>
      <button aria-label="Increment value" onClick={() => dispatch(addIssue("222"))}>
        Push
      </button>
      <button aria-label="Increment value" onClick={() => setState(state + 1)}>
        Inc
      </button>
      <About temp={projectIssues} />
      <div id="messages"></div>
      <input type="text" id="messageInput" placeholder="Type a message" />
      <button id="sendButton" onClick={onClick}>
        Send
      </button>
      {state}

      {JSON.stringify(user)}
    </div>
  );
};
