import { configureStore } from "@reduxjs/toolkit";
import IssueReducer from "../Slice";
import GithubIssueReducer from "../Thunk";
import { useDispatch } from "react-redux";

export const store = configureStore({
  reducer: {
    issue: IssueReducer,
    githubIssue: GithubIssueReducer,
  },
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
