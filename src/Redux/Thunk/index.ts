import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";

export const fetchIssues = createAsyncThunk<string, { email: string; password: string }, { rejectValue: string }>("users/login", async (user, thunkAPI) => {
  try {
    const params = new URLSearchParams(user);
    const response = await fetch(`http://localhost:3001/api/users/login?${params.toString()}`, {
      method: "POST",
    });
    const data = await response.json();
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue("Failed to fetch issues.");
  }
});

interface IssuesState {
  user: string;
  loading: boolean;
  error: string | null;
}
const initialState: IssuesState = {
  user: "",
  loading: false,
  error: null,
};
export const issuesSliceGithub = createSlice({
  name: "login",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchIssues.pending, (state) => {
        state.loading = true;
        state.error = null;
      })
      .addCase(fetchIssues.fulfilled, (state, action) => {
        state.loading = false;
        state.user = action.payload;
      })
      .addCase(fetchIssues.rejected, (state, action) => {
        state.loading = false;
        state.error = action.error.message || "Something went wrong";
      });
  },
});
export default issuesSliceGithub.reducer;
